import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '@/views/Home.vue';
import SelectFlight from '@/views/SelectFlight.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/select-flight',
    name: 'SelectFlight',
    component: SelectFlight,
  },
];

const router = new VueRouter({
  routes,
});

export default router;
