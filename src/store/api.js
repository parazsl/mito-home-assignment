const baseURL = 'https://mock-air.herokuapp.com';

async function fetchStationsData() {
  let response = await fetch(`${baseURL}/asset/stations`);
  let data = await response.json();

  return data;
}

async function searchFlights(departureStation, arrivalStation, date) {
  let response = await fetch(
    `${baseURL}/search?departureStation=${departureStation}&arrivalStation=${arrivalStation}&date=${date}`
  );
  let data = await response.json();

  return data;
}

export { fetchStationsData, searchFlights };
