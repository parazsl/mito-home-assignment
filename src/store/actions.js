import { fetchStationsData } from './api';

export default {
  loadStationsData({ commit }) {
    fetchStationsData().then((stations) => {
      commit('LOAD_STATIONS_DATA', stations);
    });
  },
};
