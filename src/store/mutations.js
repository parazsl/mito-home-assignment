export default {
  LOAD_STATIONS_DATA(state, stations) {
    state.stations = stations;
  },
};
